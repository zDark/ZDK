zDark<br>
Privacy Included<br>
Zerocoin + TOR + Segwit<br><br>
Coin: zDark<br>
Ticker: ZDK<br>
Launch: July 2018<br>
Forked from: Zcoin<br>
Total Number of Coins: 21,000,000<br>
Consensus Mechanism: PoW<br>
Hashing Algorithm: Lyra2z<br>
Block Time: 10 minutes<br>
Block Reward: 50 ZDK<br>
Masternode Collateral: 10,000 ZDK<br>
Coin Confirmation: 101 Blocks<br>
Difficulty Adjustment: Every 6 Blocks<br><br>
Website: http://zdark.io<br>
฿ ANN: https://bitcointalk.org/index.php?topic=4579776.msg41316311#msg41316311<br>
Twitter: https://twitter.com/zdk_zero<br>
Gitlab: https://gitlab.com/zDark/ZDK<br>
Discord: https://discord.gg/jgBmmuk<br>
Linux Wallet: http://zdark.io/wallets/linux/zdark_linux.tar.gz<br>
Windows Wallet: http://zdark.io/wallets/windows/zdark-qt-win.zip<br>
Apple Wallet: http://zdark.io/wallets/apple/zdark-qt-mac.zip<br><br>
Addnode: 45.77.139.132:11216<br>


